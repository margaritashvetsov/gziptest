﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Helpers.Compressor;

namespace GZipTest
{
    class Program
    {
        static void Main(string[] args)
        {
            args = new[]
            {
                "decompress",
                "compressedExpanse.avi.gz",
                "decompressedExpanse"
            };

            var command = args[0];
            var fileToProcessing = args[1];
            var resultFileName = args[2];
            if (command != "compress" && command != "decompress")
            {
                ReturnBecauseOfErrorInitialize("Invalid command. Available commands is \"compress\" and \"decompress\".");
                return;
            }

            var directoryFiles = new DirectoryInfo(Environment.CurrentDirectory).GetFiles(fileToProcessing.ToLowerInvariant());
            if (directoryFiles.Length == 0)
            {
                ReturnBecauseOfErrorInitialize("There is no file with this name in current directory.");
                return;
            }

            var regex = new Regex("^[a-zA-Zа-яА-Я0-9]*$");
            if (!regex.IsMatch(resultFileName))
            {
                ReturnBecauseOfErrorInitialize("Invalid file name. File name must exist only alphanumeric symbols.");
                return;
            }

            if (Environment.ProcessorCount < 3)
            {
                ReturnBecauseOfErrorInitialize("Sorry. This program will not work successfully because of little count of processors on you computer.");
                return;
            }

            var compressor = CompressorFactory.GetCompressor(Environment.ProcessorCount - 2);
            var fullName = directoryFiles.First().FullName;

            if (command == "compress")
            {
                var extension = fullName.Substring(fullName.IndexOf('.'));
                if (extension == ".gz")
                {
                    ReturnBecauseOfErrorInitialize("Invalid extension of file.");
                    return;
                }

                Console.WriteLine("Compressing...");
                try
                {

                    compressor.Compress(fullName,
                        Environment.CurrentDirectory 
                        + Path.DirectorySeparatorChar 
                        + resultFileName 
                        + extension);
                    Finish();
                }
                catch (Exception e)
                {
                    ReturnBecauseOfErrorInitialize("Sorry, something went wrong." + e.Message);
                }

            }

            else if (command == "decompress")
            {
                if (fullName.Substring(fullName.Length - 3) != ".gz")
                {
                    ReturnBecauseOfErrorInitialize("Invalid extension of file.");
                    return;
                }

                Console.WriteLine("Decompressing...");
                try
                {
                    compressor.Decompress(fullName,
                        Environment.CurrentDirectory
                        + Path.DirectorySeparatorChar
                        + resultFileName
                        + fullName.Remove(fullName.Length - 3).Substring(fullName.IndexOf('.')));
                    Finish();
                }
                catch (Exception e)
                {
                    ReturnBecauseOfErrorInitialize("Sorry, something went wrong." + e.Message);
                }
            }
        }

        private static void ReturnBecauseOfErrorInitialize(string message)
        {
            Console.WriteLine(message);
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        private static void Finish()
        {
            Console.WriteLine("Finish");
        }
    }
}
