﻿using System.Collections.Generic;

namespace Helpers.Queue
{
    public interface IQueue<T>
    {
        bool TryGet(out KeyValuePair<int, T> value);
        void Set(int orderedNumber, T item);
    }
}
