﻿using System;
using System.Collections.Generic;

namespace Helpers.Queue
{
    public class ThreadSafeQueue<T> : IQueue<T>
    {
        public bool TryGet(out KeyValuePair<int, T> valuePair)
        {
            valuePair = new KeyValuePair<int, T>(-1, default(T));
            lock (locker)
            {
                try
                {
                    var minIndex = lastReturnedIndex + 1;
                    if (!dict.ContainsKey(minIndex))
                    {
                        return false;
                    }

                    var value = dict[minIndex];
                    valuePair = new KeyValuePair<int, T>(minIndex, value);
                    dict.Remove(minIndex);
                    lastReturnedIndex++;
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
     
        public void Set(int orderedNumber, T item)
        {
            if (item == null)
                throw new ArgumentNullException();
            lock (locker)
            {
                try
                {
                    dict.Add(orderedNumber, item);
                }
                catch (Exception _)
                {
                    throw;
                }
            }
        }

        private Dictionary<int, T> dict = new Dictionary<int, T>();
        private object locker = new object();
        private int lastReturnedIndex = -1;
    }
}
