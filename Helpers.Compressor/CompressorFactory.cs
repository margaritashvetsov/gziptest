﻿using Compressor;

namespace Helpers.Compressor
{
    public class CompressorFactory
    {
        public static ICompressor GetCompressor(int processorCount)
        {
            return new GzipCompressor(processorCount);
        }
    }
}
