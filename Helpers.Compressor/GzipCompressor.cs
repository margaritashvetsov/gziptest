﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Threading;
using Compressor;
using Helpers.Queue;

namespace Helpers.Compressor
{
    public class GzipCompressor : ICompressor
    {
        public GzipCompressor(int processorCount = 1)
        {
            fileQueue = new ThreadSafeQueue<byte[]>();
            compressQueue = new ThreadSafeQueue<byte[]>();
            finishEvent = new ManualResetEvent(false);
            //log = File.AppendText("log.txt");
            this.processorCount = processorCount;
        }

        public void Compress(string fullPath, string resultFullPath)
        {
            finishEvent.Reset();
            //поток для чтения
            new Thread(() => SafeExecute(() => ReadFromFile(fullPath))).Start();

            //потоки для сжатия
            for (int i = 0; i < processorCount; i++)
            {
                new Thread(() => SafeExecute(() =>
                {
                    KeyValuePair<int, byte[]> block;
                    var retryCount = retryReadFromQueueCount;
                    while (true)
                    {
                        if (fileQueue.TryGet(out block))
                        {
                            retryCount = retryReadFromQueueCount;
                            if (block.Value.Length == 0)
                            {
                                try
                                {
                                    compressQueue.Set(block.Key, new byte[0]);
                                    break;
                                }
                                catch (Exception)
                                {
                                    break;
                                }
                            }

                            var compressed = CompressBytes(block.Value);
                            //log.WriteLine($"CompressBytes -> {block.Key}: from {block.Value.First()} with length {block.Value.Length} to {compressed.First()} with length {compressed.Length}");
                            compressQueue.Set(block.Key, compressed);
                        }
                        else if (retryCount > 0)
                        {
                            retryCount--;
                            Thread.Sleep(50);
                        }
                        else break;
                    }
                })).Start();
            }

            //поток для записи
            new Thread(() => SafeExecute(() =>
            {
                using (var fileInfo = File.Create(resultFullPath + ".gz"))
                {
                    WriteToStream(fileInfo, compressQueue, retryReadFromQueueCount, 50);
                    //log.Close();
                }
                finishEvent.Set();
            })).Start();
            finishEvent.WaitOne();
        }

        public void Decompress(string fullPath, string resultFullPath)
        {
            finishEvent.Reset();

            //поток для чтения
            new Thread(() => SafeExecute(() =>
            {
                var fileToDecompress = new FileInfo(fullPath);
                using (var originalFileStream = fileToDecompress.OpenRead())
                {
                    if (fileToDecompress.Extension != ".gz")
                        throw new Exception("Invalid extension of file.");

                    ReadFromGzipStream(originalFileStream, originalFileStream.Length, compressQueue);
                }
            })).Start();

            //потоки для расжатия
            for (int i = 0; i < processorCount; i++)
            {
                new Thread(() => SafeExecute(() =>
                {
                    KeyValuePair<int, byte[]> block;
                    var retryCount = retryReadFromQueueCount * 3;
                    while (true)
                    {
                        if (compressQueue.TryGet(out block))
                        {
                            retryCount = retryReadFromQueueCount * 3;
                            if (block.Value.Length == 0)
                            {
                                try
                                {
                                    fileQueue.Set(block.Key, new byte[0]);
                                    break;
                                }
                                catch (Exception)
                                {
                                    break;
                                }
                            }
                            var decompressed = DecompressBytes(block.Value);
                            //log.WriteLine($"DecompressBytes -> {block.Key}: from {block.Value.First()} with length {block.Value.Length} to {compressed.First()} with length {compressed.Length}");
                            fileQueue.Set(block.Key, decompressed);
                        }
                        else if (retryCount > 0)
                        {
                            retryCount--;
                            Thread.Sleep(50);
                        }
                        else break;
                    }
                })).Start();
            }
            new Thread(() => SafeExecute(() =>
            {
                using (var decompressedFileStream = File.Create(resultFullPath))
                {
                    WriteToStream(decompressedFileStream, fileQueue, retryReadFromQueueCount * 3, 200);
                    //log.Close();
                }
                finishEvent.Set();
            })).Start();
            finishEvent.WaitOne();
        }

        #region compress
        private void ReadFromFile(string fullPath)
        {
            var fileInfo = new FileInfo(fullPath);
            using (var originalFileStream = fileInfo.OpenRead())
            {
                if ((File.GetAttributes(fileInfo.FullName) &
                     FileAttributes.Hidden) != FileAttributes.Hidden & fileInfo.Extension != ".gz")
                {
                    ReadFromStream(originalFileStream, originalFileStream.Length, fileQueue);
                }
                else throw new Exception("Invalid extension of file.");
            }
        }

        private void ReadFromStream(Stream stream, long streamLength, IQueue<byte[]> queue)
        {
            try
            {
                var bufferLength = 1048576;
                var lastBlockLength = streamLength % bufferLength;
                var maxN = streamLength / bufferLength;
                for (var n = 0; n <= maxN; n++)
                {
                    if (n == maxN)
                    {
                        if (lastBlockLength != 0)
                            bufferLength = (int)lastBlockLength;
                        else break;
                    }

                    var buffer = new byte[bufferLength];
                    stream.Read(buffer, 0, buffer.Length);
                    queue.Set(n, buffer);
                }

                queue.Set((int) maxN + 1, new byte[0]);
            }
            catch (Exception _)
            {
                throw;
            }
        }

        private byte[] CompressBytes(byte[] input)
        {
            using (var result = new MemoryStream())
            {
                using (var compressionStream = new GZipStream(result, CompressionMode.Compress))
                {
                    compressionStream.Write(input, 0, input.Length);
                    compressionStream.Flush();
                }

                return result.ToArray();
            }
        }

        #endregion

        #region decompress
        private void ReadFromGzipStream(Stream stream, long streamLength, IQueue<byte[]> queue)
        {
            var blocksCount = 0;
            var header = new byte[10] { 31, 139, 8, 0, 0, 0, 0, 0, 4, 0 };
            //var header = new byte[8] { 31, 139, 8, 0, 0, 0, 0, 0 };
            var bytes = new List<byte>();
            byte[] currentTenBytes = new byte[header.Length];
            stream.Read(currentTenBytes, 0, header.Length);
            bytes.AddRange(currentTenBytes);
            var readBytesCount = header.Length;

            while (readBytesCount < streamLength)
            {
                if (currentTenBytes.SequenceEqual(header) && bytes.Count > header.Length)
                {
                    queue.Set(blocksCount, bytes.Take(bytes.Count - header.Length).ToArray());
                    blocksCount++;
                    bytes = new List<byte>();
                    bytes.AddRange(currentTenBytes);
                }

                var nextByte = stream.ReadByte();
                readBytesCount++;
                currentTenBytes = ShiftByteArray(currentTenBytes, (byte)nextByte);
                bytes.Add((byte)nextByte);
            }

            queue.Set(blocksCount, bytes.ToArray());
            queue.Set(blocksCount + 1, new byte[0]);
        }

        private byte[] ShiftByteArray(byte[] byteArray, byte lastElement)
        {
            var newByteArray = new byte[byteArray.Length];
            for (int i = 0; i < byteArray.Length - 1; i++)
            {
                newByteArray[i] = byteArray[i + 1];
            }

            newByteArray[byteArray.Length - 1] = lastElement;
            return newByteArray;
        }

        private byte[] DecompressBytes(byte[] input)
        {
            using (var source = new MemoryStream(input))
            {
                using (var decompressionStream = new GZipStream(source, CompressionMode.Decompress))
                {
                    var result = new List<byte>();
                    int read;
                    byte[] buffer = new byte[1024];
                    while ((read = decompressionStream.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        result.AddRange(buffer.Take(read));
                    }

                    return result.ToArray();
                }
            }
        }

        #endregion

        private void WriteToStream(Stream stream, IQueue<byte[]> queue, int retryReadCount, int readDelay)
        {
            KeyValuePair<int, byte[]> block;
            var retryCount = retryReadCount;
            while (true)
            {
                if (queue.TryGet(out block))
                {
                    retryCount = retryReadCount;
                    if (block.Value == new byte[0])
                        break;
                    //log.WriteLine($"WriteToStream -> write {block.Key} starts with {block.Value.First()}");
                    stream.Write(block.Value, 0, block.Value.Length);
                }
                else if (retryCount > 0)
                {
                    retryCount--;
                    Thread.Sleep(readDelay);
                }
                else break;
            }
        }

        private static void SafeExecute(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private readonly IQueue<byte[]> fileQueue;
        private readonly IQueue<byte[]> compressQueue;
        private readonly ManualResetEvent finishEvent;
        //private StreamWriter log;
        private int processorCount;
        private int retryReadFromQueueCount = 10;
    }
}
