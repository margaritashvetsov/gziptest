﻿namespace Compressor
{
    public interface ICompressor
    {
        void Compress(string fullPath, string resultFullPath);
        void Decompress(string fullPath, string resultFullPath);
    }
}
